clc
clear all
disp('Matlab started')

x = 1:100;

y = x.^2;

out = sum(y);


disp('Matlab finished')
disp(['Result: ', num2str(out)])

exit;